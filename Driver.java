import java.util.Scanner;
public class Driver {
	public static void foo(){
		//nothing
	}
	public static void main(String[] args){
		System.out.println("Hello there! ");
		Scanner reader = new Scanner(System.in);
		System.out.println("Enter 2 numbers ");
		double userInput1 = reader.nextDouble();
		double userInput2 = reader.nextDouble();
		double sum = Calculator.addNum(userInput1, userInput2);
		System.out.println("The sum of " + userInput1 + " and " + userInput2 + " is " + sum);
		System.out.println("Enter a new number ");
		int userInput3 = reader.nextInt();
		double Root = Calculator.squareRoot(userInput3);
		System.out.println("The square root of " + userInput3 + " is " + Root);
		double random = Calculator.randomNum();
		System.out.println("Random number is " + random);
		System.out.println("Does pushing without pulling cause an error?");
		//nothing
		System.out.println("What happens if I push without pulling");
		//nothing
	}
	public static void foo(){
		System.out.println("Hello");
	}
}