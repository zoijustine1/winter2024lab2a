import java.util.Random;
public class Calculator {
	public static double addNum(double x, double y){
		double sum = x + y;
		return sum;
	}
	public static double squareRoot(double x){
		double rootNum = Math.sqrt(x);
		return rootNum;
	}
	public static double randomNum(){
		Random rand = new Random();
		int randomNum = rand.nextInt(1000) + 1;
		return randomNum;
	}
	public static double divideNum(double numerator, double denominator){
		if(denominator == 0){
			System.out.println("Dividing by 0 gives an error");
			return -1;
		}
		double result = numerator/denominator;
		return result;
	}
			
}